#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('/home/poigal/assig/clipboard/src')
import clipboard

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = 'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        assert(msg == text)

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("Yeee, Haww!")
        text = clipboard.gettext()
        assert(text == "Yeee, Haww!")

    def test_copy_text_text():
        clipboard.reset()
        clipboard.copytext("Yeee,")
        clipboard.copytext(" Haww!")
        text = clipboard.gettext()
        assert(text == " Haww!")

    def test_copy_blob():
        clipboard.reset()
        clipboard.copyblob([1,1,1,1,1,1,1,1,1,1])
        blob = clipboard.getblob()       
        assert(blob == [1,1,1,1,1,1,1,1,1,1])

    def test_copy_blob_blob():
        clipboard.reset()
        clipboard.copyblob([1,1,0,0])
        clipboard.copyblob([1,1,1,1])
        blob = clipboard.getblob()
        assert(blob == [1,1,1,1])
    
    def test_copy_text_blob():
        clipboard.reset()
        clipboard.copytext("Yeee")
        clipboard.copyblob([1,1,0,0])
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "Yeee")
        assert(blob == [1,1,0,0])

    def test_copy_blob_text():
        clipboard.reset()
        clipboard.copyblob([1,1,1,1])
        clipboard.copytext("Haww")
        blob = clipboard.getblob()
        text = clipboard.gettext()
        assert(blob == [1,1,1,1])
        assert(text == "Haww")

    def test_copy_text_reset():
        clipboard.reset()
        clipboard.copytext("Yeee")
        clipboard.reset()
        text = clipboard.gettext()
        assert(text == None)

    def test_copy_blob_reset():
        clipboard.reset()
        clipboard.copyblob([1,1,1,1])
        clipboard.reset()
        blob = clipboard.getblob()
        assert(blob == None)
    
    def test_copy_text_blob_text():
        clipboard.reset()
        clipboard.copytext("Yeee,")    
        clipboard.copyblob([1,1,1,1])           
        clipboard.copytext("  Haww!")
        text = clipboard.gettext()
        blob = clipboard.getblob()        
        assert(text == "  Haww!")
        assert(blob == [1,1,1,1])

    def test_copy_text_blob_blob():
        clipboard.reset()
        clipboard.copytext("Yeee")
        clipboard.copyblob([1,1,0,0])
        clipboard.copyblob([1,1,1,1])
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "Yeee")
        assert(blob == [1,1,1,1])

    def test_copy_text_text_blob():
        clipboard.reset()
        clipboard.copytext("Yeee,")        
        clipboard.copytext(" Haww!")        
        clipboard.copyblob([1,1,1,1])
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == " Haww!")
        assert(blob == [1,1,1,1])

    def test_copy_blob_blob_text():
        clipboard.reset()
        clipboard.copyblob([1,1,0,0])
        clipboard.copyblob([1,1,1,1])            
        clipboard.copytext("Yeee!")
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "Yeee!")
        assert(blob == [1,1,1,1])
        

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    test_copy_hindi_text()
    test_copy_text_text()
    test_copy_blob()
    test_copy_blob_blob()
    test_copy_text_blob()
    test_copy_blob_text()   
    test_copy_text_blob_text()
    test_copy_text_blob_blob()
    test_copy_text_text_blob()
    test_copy_blob_blob_text()
    test_copy_text_reset()
    test_copy_blob_reset()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("Hello, World!")
    
    test_one_observer()

run_clipboard_observer_tests()
