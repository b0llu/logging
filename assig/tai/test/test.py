#!/usr/bin/env python
import sys
sys.path.append('/home/poigal/assig/tai/src')
import tai

def run_tai_tests():
    def test_process_1():
        assert(tai.process('test.txt')==['w','c'])
    
    def test_process_2():
        tai.process('test2.txt')
        assert(tai.process('test2.txt')==['c', 'w', 'c', 'c', 'w', 'w', 'w'])
    
    test_process_1()
    test_process_2() 
    
run_tai_tests()


