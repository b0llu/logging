import tornado.ioloop
import tornado.web
import Logging


class DebugHandler(tornado.web.RequestHandler):
    def get(self, log_message):
        Logging.LOGGER.debug(':message:'+str(log_message))
        
class InfoHandler(tornado.web.RequestHandler):
    def get(self, log_message):
        Logging.LOGGER.info(':message:'+str(log_message))  
 
class WarningHandler(tornado.web.RequestHandler):
    def get(self, log_message):
        Logging.LOGGER.warning(':message:'+str(log_message))  
 
class ErrorHandler(tornado.web.RequestHandler):
    def get(self, log_message):
        Logging.LOGGER.error(':message:'+str(log_message))  
 
class CriticalHandler(tornado.web.RequestHandler):
    def get(self, log_message):
        Logging.LOGGER.critical(':message:'+str(log_message))  




application = tornado.web.Application([        
    (r"/info/(.*)", InfoHandler),
    (r"/warning/(.*)",WarningHandler),
    (r"/error/(.*)", ErrorHandler),
    (r"/critical/(.*)", CriticalHandler),
    (r"/debug/(.*)", DebugHandler),
    (r"/(.*)", DebugHandler),
])

if __name__ == "__main__":     
    application.listen(8239)
    tornado.ioloop.IOLoop.instance().start()
