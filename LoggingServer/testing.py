import requests

def run_LoggingServer_tests():

    def test_critical():         
        a=repr(requests.get('http://127.0.0.1:8239/critical/Test Passed for Critical'))
        assert(a=='<Response [200]>')
        
    def test_error():
        a=repr(requests.get('http://127.0.0.1:8239/error/Test Passed for Error'))
        assert(a=='<Response [200]>')
    
    def test_warning():
        a=repr(requests.get('http://127.0.0.1:8239/warning/Test Passed for Warning'))
        assert(a=='<Response [200]>')
        
    def test_info():
        a=repr(requests.get('http://127.0.0.1:8239/info/Test Passed for Info'))
        assert(a=='<Response [200]>')
        
    def test_debug():
        a=repr(requests.get('http://127.0.0.1:8239/debug/Test Passed for Debug'))
        assert(a=='<Response [200]>')
        
    def test_notset():
        a=repr(requests.get('http://127.0.0.1:8239/Test Passed for Notset'))
        assert(a=='<Response [200]>')
        
    test_critical()
    test_error()
    test_warning()
    test_info()
    test_debug()
    test_notset()    
        
run_LoggingServer_tests()

